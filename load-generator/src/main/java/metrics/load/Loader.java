package metrics.load;

import io.micronaut.context.annotation.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Stresser for the http endpoint.
 *
 * @author Enrique Zamudio
 * Date: 2019-06-21 17:40
 */
@Singleton @Context
public class Loader {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final ExecutorService threadPool = Executors.newFixedThreadPool(16);
    private final Random rng = new Random(System.currentTimeMillis());

    @Inject
    Cliente cliente;

    @PostConstruct
    public void run() {
        log.info("Fire!");
        //Weird bug; if I don't "prime" the client by calling it from here,
        //the tasks never run
        log.info("Test: {}", cliente.hash("test"));
        while (true) {
            int limit = rng.nextInt(50)+1;
            for (int i = 0; i < limit; i++) {
                threadPool.execute(this::task);
            }
            try {
                int time = rng.nextInt(500) + 10;
                if (time > 50) {
                    log.info("Sleeping {} before next batch", time);
                }
                Thread.sleep(time);
            } catch (InterruptedException ex) {
                //ni modo gracias
            }
        }
    }

    public void task() {
        char[] letras = new char[rng.nextInt(512) + 32];
        for (int i = 0; i < letras.length; i++) {
            letras[i] = Character.toChars(rng.nextInt(100) + 32)[0];
        }
        try {
            String r = cliente.hash(new String(letras));
        } catch (Exception ex) {
            log.error("ouch", ex);
        }
    }
}
