package metrics.load;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

/**
 * Hashing client.
 *
 * @author Enrique Zamudio
 * Date: 2019-06-21 17:39
 */
@Client("http://localhost:8888/jvmmx")
public interface Cliente {

    @Get("/hash/{value}")
    String hash(String value);
}
