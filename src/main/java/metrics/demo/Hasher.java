package metrics.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Random;

/**
 * Hashing component.
 *
 * @author Enrique Zamudio
 * Date: 2019-06-21 19:13
 */
@Singleton
public class Hasher {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final Random rng = new Random(System.currentTimeMillis());
    private final long start = System.currentTimeMillis();

    public String hash(String something) {
        if (rng.nextInt() % 20 == 0 && System.currentTimeMillis() - start > 120_000) {
            throw new RuntimeException("Boom!");
        }
        try {
            var md5 = MessageDigest.getInstance("SHA-256");
            md5.update(something.getBytes("UTF-8"));
            return Base64.getEncoder().encodeToString(md5.digest());
        } catch (GeneralSecurityException | UnsupportedEncodingException ex) {
            log.error("No pude generar el hash para '{}'", something, ex);
            return null;
        }
    }
}
