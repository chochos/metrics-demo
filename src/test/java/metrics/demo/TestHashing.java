package metrics.demo;

import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

/**
 * Simple test.
 *
 * @author Enrique Zamudio
 * Date: 2019-06-21 17:22
 */
@MicronautTest
public class TestHashing {

    @Inject
    HashingClient client;

    @Test
    void testHashing() {
        Assertions.assertEquals("siHZ27CDp/M0KNfCo8MZiuklYU1wIQ4ocWzKp81N23k=", client.hash("hola"));
        Assertions.assertEquals("6AUJa09e7jE1UDkExOsJml9RxYJ8uhz/TYqJHAcbnbk=", client.hash("jvmmx"));
        Assertions.assertEquals("N6/hjf9S5Lhw/X62bdjhPaLIArIpwYAhk4m4DZPDzFI=", client.hash("JVMMX"));
        Assertions.assertEquals("toCzJJBUCRasCIg7osb5TwkegQPP8cGsUvrhXMixNp4=", client.hash("WTF"));
        Assertions.assertEquals("DWJi/vjf56tPoq0T+W8fzE1NAhbTT7HGEap5j+iOfTo=", client.hash("un hash de algo más o menos largo"));
    }
}
