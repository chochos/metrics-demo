package metrics.demo;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

/**
 * A simple http client.
 *
 * @author Enrique Zamudio
 * Date: 2019-06-21 17:24
 */
@Client("http://localhost:8888/jvmmx")
public interface HashingClient {

    @Get("/hash/{string}")
    String hash(String string);
}
