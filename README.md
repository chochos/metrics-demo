This is a simple demo service to illustrate the introduction of metrics
using the micrometer library.

The master branch has is the original, simple version of the service.

The metrics branch is basically the same code but it has metrics.

##Running

The `load-generator` directory contains a simple program that connects
to the demo service and generates a lot of traffic.

There's a docker-compose file in the grafana+graphite directory to
start up those services. When you run the service from the `metrics`
branch, you'll be able to create some graphs and dashboards in Grafana.

Log in to Grafana at http://localhost:3000/ as admin/pass.
